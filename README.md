# Deep Learning

Toda la práctica se ha realizado en un único notebook que es el que se ha dejado en el repositorio gitlab con el nombre de "Practica_Deep_Learning". La práctica tiene una estructura bastante clara, dividida en diferentes secciones pero principalmente tiene estas partes:

* Carga datos fichero csv e imagenes-> En esta parte obtenemos los 2 datasets que vamos a utilizar a lo largo de la Practica
* Prepocesado de datos y análisis exploratorio: En esta parte se va a realizar la limpieza de datos, missings, quitar variables, y ver correlaciones. Al final nos quedaremos con el dataset que vamos a trabajar en el resto del documento.
* Datos Fichero CSV:
    * Modelo regresión y clasificación
* Datos Imagenes: 
    * Modelo regresión y clasificación (Redes Convolucionales)
*Datos fichero CSV + Datos Imagenes 
    * Modelo regresión y clasificación
